# Configurar Servidor FTP con vsftpd en Ubuntu 16.04

## Introduction

__FTP__ _(File Transfer Protocol)_, es un protocolo de red que se utiliza para mover ficheros de un cliente a un servidor y viceversa.

## Instalar vsftpd

Instalación del paquete vsftpd:

```bash
$ sudo apt-get update
$ sudo apt-get install vsftpd
```

Cuando acabe el proceso de instalación, hacemos una copia de seguridad del fichero de configuración antes de editarlo:

```bash
$ sudo cp /etc/vsftpd.conf /etc/vsftpd.conf.orig
```

A continuación se muestra una lista de puertos necesarios para abrir en el caso de que el servidor esté detrás de un firewall o en una red tipo NAT:

```bash
$ sudo ufw allow 20/tcp
$ sudo ufw allow 21/tcp
$ sudo ufw allow 990/tcp
$ sudo ufw allow 40000:50000/tcp
$ sudo ufw status
```

## Preparar el directorio de usuario

A continuación crearemos un usuario del sistema y le daremos permisos para que pueda conectarse mediante FTP:

```bash
$ sudo adduser sammy
```

Asignar un password por consola, el resto de campos dejarlos en blanco.

FTP es más seguro cuando se restringe el acceso por FTP a un directorio específico. __vsftp__ permite hacer esto mediante el uso de _chroot jails_. Cuando chroot está habilitado para usuarios locales, están restringidos a su directorio de inicio de manera predeterminada. Sin embargo, debido a la forma en que vsftpd protege el directorio, el usuario no debe poder escribirlo. Esto está bien para un nuevo usuario que solo debe conectarse a través de FTP, pero un usuario existente puede necesitar escribir en su carpeta de inicio si también tiene acceso al shell.

En este ejemplo, en lugar de eliminar los privilegios de escritura del directorio principal, crearemos un directorio ftp para que sirva como chroot y un directorio de archivos modificables para contener los archivos reales.

Creamos una carpeta en el home del usuario que se llame _ftp_, configuramos los permisos y nos aseguramos de eliminar los permisos de escritura con los siguientes comandos::

```bash
$ sudo mkdir /home/sammy/ftp
$ sudo chown nobody:nogroup /home/sammy/ftp
$ sudo chmod a-w /home/sammy/ftp
```

Para verificar los permisos:

```bash
$ sudo ls -la /home/sammy/ftp

Output
total 8
4 dr-xr-xr-x  2 nobody nogroup 4096 Aug 24 21:29 .
4 drwxr-xr-x 3 sammy  sammy   4096 Aug 24 21:29 ..
```

Creamos un directorio donde _ftp_ donde vamos a poder subir ficheros:

```bash
$ sudo mkdir /home/sammy/ftp/files
$ sudo chown sammy:sammy /home/sammy/ftp/files
```

Comprobamos los permisos:

```bash
$ sudo ls -la /home/sammy/ftp

Output
total 12
dr-xr-xr-x 3 nobody nogroup 4096 Aug 26 14:01 .
drwxr-xr-x 3 sammy  sammy   4096 Aug 26 13:59 ..
drwxr-xr-x 2 sammy  sammy   4096 Aug 26 14:01 files
```

Finalmente, añadimos un fichero llamado __test.txt__ que usaremos posteriormente para realizar pruebas:

```bash
$ echo "vsftpd test file" | sudo tee /home/sammy/ftp/files/test.txt
```

Hemos asegurado el directorio _ftp_ para que no se pueda escribir en él y hemos habilitado un directorio dentro del directorio _ftp_ llamado _files_ con permisos de escritura para poder escribir en él.

## Configuración de acceso al FTP

Vamos a permitir a los usuarios del sistema acceder por FTP al servidor usando sus cuentas de usuario. Para ello configuraremos servidor __vsftpd__ de la siguiente manera:


```bash
$ sudo nano /etc/vsftpd.conf
```

```bash
. . .
# Allow anonymous FTP? (Disabled by default).
anonymous_enable=NO
#
# Uncomment this to allow local users to log in.
local_enable=YES
. . .
```

Permitir que un usuario pueda subir ficheros, esta opción viene desactivada por defecto:

```bash
. . .
write_enable=YES
. . .
```

Para prevenir que un usuario que se conecte por FTP pueda acceder a cualquier fichero fuera del directorio _ftp_. Esto se controla con el comando _chroot_:

```bash
. . .
chroot_local_user=YES
. . .
```

Necesitamos añadir un _user_sub_token_ para insertar el usaurio en la ruta de nuestro directorio _local_root_:

```bash
user_sub_token=$USER
local_root=/home/$USER/ftp
```

Limiar el rango de puertos que pueden usados para FTP en modo pasivo:


```bash
pasv_min_port=40000
pasv_max_port=50000
```

> __Nota__: Este rango de puertos son los que indicamos anteriormente  en el caso de que el servidor esté detrás de un firewall o en una red tipo NAT.

Vamos a indicar un fichero en el que se insertarán aquellos usuarios a los que permitiremos el acceso por FTP:

```bash
userlist_enable=YES
userlist_file=/etc/vsftpd.userlist
userlist_deny=NO
```

> __Nota__: Cuando __userlist_deny__ se configura a "YES", los usuarios que están en el fichero _/etc/vsftpd.userlist_ no pueden acceder por FTP. Cuando está configurado a "YES", sólo los usuarios que están en el fichero tienen acceso mediante FTP.

Finalmente, añadimos el usuario creado anteriormente al fichero, con el flag -a para añadir al fichero:

```bash
$ echo "sammy" | sudo tee -a /etc/vsftpd.userlist
```

Imprimimos el contenido del fichero:

```bash
cat /etc/vsftpd.userlist
sammy
```

Reiniciamos el servicio del vsftpd para que se apliquen los cambios:

```bash
$ sudo systemctl restart vsftpd
```

## Accediento al FTP

We've configured the server to allow only the user sammy to connect via FTP. Let's make sure that's the case.

Probamos la conexión al ftp:

```bash
$ ftp -p 203.0.113.0

Output
Connected to 203.0.113.0.
220 (vsFTPd 3.0.3)
Name (203.0.113.0:default): sammy
331 Please specify the password.
Password: your_users_password
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp>
```

Ahora nos situamos en el directorio _files_ y descargamos el fichero _test.txt_:

```bash
cd files
get test.txt

Output
227 Entering Passive Mode (203,0,113,0,169,12).
150 Opening BINARY mode data connection for test.txt (16 bytes).
226 Transfer complete.
16 bytes received in 0.0101 seconds (1588 bytes/s)
ftp>
```

Ahora probaremos subir de nuevo el fichero _test.txt_ pero con el nombre _upload.txt_:

```bash
put test.txt upload.txt

Output
227 Entering Passive Mode (203,0,113,0,164,71).
150 Ok to send data.
226 Transfer complete.
16 bytes sent in 0.000894 seconds (17897 bytes/s)
```

## Asegurando las Transacciones

FTP no encripta ningún tipo de dato, incluyendo las credenciales del usuario, así que habilitaremos en el servidor el cibrado TTL/SSL. El primer paso es crear los certificados SSL para usarlos con vsftpd.

Usaremos __openssl__ para crear un nuevo certificado y usaremos el flag _-days_ para hacer el certificado válido por un año. En el mismo comando añadiremos una clave RSA de 2048 bit privada. Configrando los flags _-keyout_ y _-out_ al mismo valor, la clave privada y el certificado estarán localizados en el mismo fichero:

```bash
$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/vsftpd.pem -out /etc/ssl/private/vsftpd.pem

Generating a 2048 bit RSA private key
............................................................................+++
...........+++
writing new private key to '/etc/ssl/private/vsftpd.pem'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:US
State or Province Name (full name) [Some-State]:NY
Locality Name (eg, city) []:New York City
Organization Name (eg, company) [Internet Widgits Pty Ltd]:DigitalOcean
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:
Email Address []:
```

Una vez creados los certificados, abrimos el fichero de configuración de vsftpd de nuevo y configuramos los parámetros _rsa_cert_file_ y _rsa_private_key_file_ indicándole al servidor FTP dónde está el certificado y la clave privada respectivamente:

```bash
$ sudo nano /etc/vsftpd.conf
...
rsa_cert_file=/etc/ssl/private/vsftpd.pem
rsa_private_key_file=/etc/ssl/private/vsftpd.pem
...
```

Una vez hecho esto, forzaremos el uso de _SSL_, lo que evitará que los clientes que no puedan tratar con _TTL_ no se conecten. Cambiamos el parámetro _ssl_enable_ to _YES_:

```bash
$ sudo nano /etc/vsftpd.conf
...
ssl_enable=YES
...
```

Después de esto, añadimos las siguientes líneas para denegar las conexiones anónimas sobre SSL y para requerir SSL tanto para la transferencia de datos como para los logins de los usuarios:

```bash
$ sudo nano /etc/vsftpd.conf
...
allow_anon_ssl=NO
force_local_data_ssl=YES
force_local_logins_ssl=YES
...
```

Posteriormente, configuramos el servidor para que use TLS:

```bash
$ sudo nano /etc/vsftpd.conf
...
ssl_tlsv1=YES
ssl_sslv2=NO
ssl_sslv3=NO
...
```

Finalmente, añadimos dos opciones. La primera, no necesitamos _require ssl_ porque puede romper muchas conexiones de clientes FTP. Y la segunda, requiere cifrados de encriptación elevadas, es decir, claves con una longitud igual o superior a 128 bits:

```bash
$ sudo nano /etc/vsftpd.conf
...
require_ssl_reuse=NO
ssl_ciphers=HIGH
...
```

Guardamos el fichero y reiniciamos el servidor para que los cambios tengan efecto:

```bash
$ sudo systemctl restart vsftpd
```

En este punto, no deberíamos poder conectarnos al servidor FTP usando un cliente insegudo:

```bash
    ftp -p 203.0.113.0
    Connected to 203.0.113.0.
    220 (vsFTPd 3.0.3)
    Name (203.0.113.0:default): sammy
    530 Non-anonymous sessions must use encryption.
    ftp: Login failed.
    421 Service not available, remote server has closed connection
    ftp>
```

## Acceder al servidor FTP usando una conexión TLS con FileZilla

La mayoría de clientes FTP modernos pueden configurarse para usar encriptación TLS.


Si pulsamos sobre el botón _My Sites_ se abrirá el panel de Gestor de sitios. Para configurar una conexión con TLS, debemos añadir las siguientes opciones en la pestaña _General_:

* __Protocolo__: FTP - Protocolo de Transferencia de Archivos.
* __Cifrado__: Use explicit FTP over TLS if available.
* 

## Realizando una conexión FTP sobre una conexión TLS mediante código Java

FTPS extiende el protocolo FTP con soporte para SSL y TLS.

El siguiente ejemplo usa la librería [Jakarta Commons Net](https://commons.apache.org/proper/commons-net/) y asegura el login y la transferencia de datos usando TLS.

Añadir la librería a nuestro proyecto usando Maven:

```bash
<!-- https://mvnrepository.com/artifact/commons-net/commons-net -->
<dependency>
    <groupId>commons-net</groupId>
    <artifactId>commons-net</artifactId>
    <version>3.3</version>
</dependency>
```

```java
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

public class FTPSTest {

  public void putFile(String host,
                      int port,
                      String username,
                      String password,
                      String localFilename,
                      String remoteFilename) {
    try {
      FTPSClient ftpClient = new FTPSClient(false);
      // Connect to host
      ftpClient.connect(host, port);
      int reply = ftpClient.getReplyCode();
      if (FTPReply.isPositiveCompletion(reply)) {

        // Login
        if (ftpClient.login(username, password)) {

          // Set protection buffer size
          ftpClient.execPBSZ(0);
          // Set data channel protection to private
          ftpClient.execPROT("P");
          // Enter local passive mode
          ftpClient.enterLocalPassiveMode();

          // Store file on host
	  InputStream is = new FileInputStream(localFilename);
	  if (ftpClient.storeFile(remoteFilename, is)) {
	    is.close();
	  } else {
	    System.out.println("Could not store file");
	  }
	  // Logout
	  ftpClient.logout();

        } else {
          System.out.println("FTP login failed");
        }

        // Disconnect
    	ftpClient.disconnect();

      } else {
        System.out.println("FTP connect to host failed");
      }
    } catch (IOException ioe) {
      System.out.println("FTP client received network error");
    } catch (NoSuchAlgorithmException nsae) {
      System.out.println("FTP client could not use SSL algorithm");
    }
  }
}
```

Resultado de la ejecución:

```bash
The resulting FTP server log

CONNECT [     0] - Incoming connection request
CONNECT [     0] - FTP Connection request accepted
COMMAND [     0] - AUTH TLS
  REPLY [     0] - 234 Authentication method accepted

CONNECT [     0] - SSL connection using TLSv1/SSLv3 (RC4-MD5)
CONNECT [     0] - SSL connection established
COMMAND [     0] - USER test
  REPLY [     0] - 331 User test, password please

COMMAND [     0] - PASS ***********
CONNECT [     0] - Native user 'test' authenticated
  REPLY [     0] - 230 Password Ok, User logged in

COMMAND [     0] - PBSZ 0
  REPLY [     0] - 200 PBSZ=0

COMMAND [     0] - PROT P
  REPLY [     0] - 200 PROT P OK, data channel will be secured

COMMAND [     0] - PASV
  REPLY [     0] - 227 Entering Passive Mode (127,0,0,1,43,41)

COMMAND [     0] - STOR test.txt
  REPLY [     0] - 150 Opening data connection

CONNECT [     0] - SSL connection using TLSv1/SSLv3 (RC4-MD5)
CONNECT [     0] - SSL data connection established
 SYSTEM [     0] - Successfully stored file at 'c:\ftp\test.txt'
  REPLY [     0] - 226 Transfer complete

COMMAND [     0] - QUIT
CONNECT [     0] - Connection terminated
```